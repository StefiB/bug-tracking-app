package services;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import bts.bugtracker.model.Status;
import bts.bugtracker.service.StatusService;
import bts.bugtracker.service.impl.JpaStatusService;

public class InMemoryStatusServiceTest {

	private StatusService statusServ;
	
	@Before
	public void setUp() {
		statusServ = new JpaStatusService();
	}
	
	@Test
	public void testFindOne() {
		Status status = statusServ.findOne(0);
		Assert.assertNotNull(status);
		Assert.assertEquals(Status.NEW, status);
	}
	
	@Test
	public void testFindAll() {
		List<Status> statuses = statusServ.findAll();
		Assert.assertNotNull(statuses);
		Assert.assertEquals(3, statuses.size());
	}
}
