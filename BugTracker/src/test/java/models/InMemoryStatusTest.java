package models;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import bts.bugtracker.model.Status;

public class InMemoryStatusTest {
	
	 private Status s1, s2, s3, s4;
	 private String closed = "closed";
	 private String undefined = "UNDEFined";

	@Before
	public void setUp() {
		s1 = Status.NEW;
		s2 = Status.IN_PROGRESS;	
	}
	
	@Test
	public void testFromValue() {
		s3 = Status.fromValue(closed);
		Assert.assertNotNull(s3);
		Assert.assertEquals(Status.CLOSED, s3);
	}
	
	@Test
	public void testFromValueWithMixedCases() {
		s4 = Status.fromValue(undefined);
		Assert.assertNotNull(s4);
		Assert.assertEquals(Status.UNDEFINED, s4);
	}
	
	@Test
	public void testNextStatus() {
		Status st = s1.nextStatus();
		Assert.assertNotNull(st);
		Assert.assertEquals(Status.IN_PROGRESS, st);
	}
	
	@Test
	public void testNextStatusWhenEqualsToClose() {
		s3 = Status.CLOSED;
		Exception thrown = Assert
				.assertThrows(
						java.lang.UnsupportedOperationException.class, () -> s3.nextStatus()
				);
		
		Assert.assertEquals("Illegal request:\n\tImposible further STATUS iterations!", thrown.getMessage());
	}
	
	@Test
	public void getAllStatusesAsList() {
		List<Status> all = Status.orderedValues;
		Assert.assertNotNull(all);
		Assert.assertEquals(4, all.size());
	}
}
