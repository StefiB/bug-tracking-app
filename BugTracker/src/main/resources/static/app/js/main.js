var btsApp = angular.module("btsApp", ["ngRoute", "ui.bootstrap.modal"]);

btsApp.controller("HomeCtrl", function($scope) {
	$scope.message = "Welcome to Bug Tracking Service!";
});

btsApp.controller("ShareholdersCtrl", function($scope, $http, $location) {
	var url = "/api/shareholders";
	
	$scope.shareholders = [];
	
	var getShareholders = function() {
		var promise = $http.get(url);
		promise.then(
				function success(res) {
					$scope.shareholders = res.data;
				},
				function error(res) {
					alert("Couldn't fetch shareholders!");
				}
		);
	}
	
	getShareholders();
	
	$scope.goToAdd = function() {
		$location.path("/shareholders/add");
	}
	
	$scope.goToEdit = function(id) {
		$location.path("/shareholders/edit/" + id);
	}
	
	$scope.doDelete = function(id) {
		var promise = $http.delete(url + "/" + id);
		promise.then(
				function success() {
					getShareholders();
				},
				function error() {
					alert("Couldn't delete project!");
				}
		);
	}
});

btsApp.controller("AddShareholderCtrl", function($scope, $http, $location) {
	var shareholderUrl = "/api/shareholders/";
	var projectsUrl = "/api/projects";
	var bugsUrl = "/api/bugs";
	
	$scope.shareholder = {};
	$scope.shareholder.id = "";
	$scope.shareholder.name = "";
	$scope.shareholder.surname = "";
	$scope.shareholder.userName = "";
	$scope.shareholder.password = "";
	$scope.shareholder.role = "";
	$scope.shareholder.projectIds = [];
	
	$scope.projects = [];
// $scope.bugs = [];
	
	var getProjects = function() {
		$http.get(projectsUrl).then(
				function success(res) {
					$scope.projects = res.data;
//					getShareHolder();
				},
				function error(res) {
					alert("Couldn't fetch projects!");
				}
		);
	}
	
//	var getShareHolder = function() {
//		$http.get(shareholderUrl).then(
//				function success(res) {
//					$scope.shareholder = res.data;
//				},
//				function error(res) {
//					alert("Couldn't fetch shareholder!");
//				}
//		);
//	}

	getProjects();
	
	var newArray = [];
	
	$scope.iterateProjectIds = function(pid) {
		var retVal = false;
		
		if($scope.shareholder.projectIds != null) {
			for(var i = 0, l = $scope.shareholder.projectIds.length; i < l; i++) {

				if(pid == $scope.shareholder.projectIds[i]) {
					retVal = true;
					newArray.push(pid);
				}
			}
		}
		
		return retVal;
	};
	
	$scope.pushSplice = function(pid) {
		
		if($scope.shareholder.projectIds == null) {
			$scope.shareholder.projectIds = [];
			$scope.shareholder.projectIds[0] = pid;
		} else {
			var i = $scope.shareholder.projectIds.indexOf(pid);
			
			if(i != -1) {
				$scope.shareholder.projectIds.splice(i, 1);
			} else {
				$scope.shareholder.projectIds.push(pid);
			}
		}
	}
	
	$scope.doAdd = function() {		
		$http.post(shareholderUrl, $scope.shareholder).then(
				function success() {
					$location.path("/shareholders");					
				},
				function error() {
					alert("Something went wrong!");
				}
		);
	}
	
});

btsApp.controller("EditShareholderCtrl", function($scope, $http, $routeParams, $location) {
	var shareholderUrl = "/api/shareholders/" + $routeParams.shid;
	var projectsUrl = "/api/projects";
	var bugsUrl = "/api/bugs";
	
	$scope.shareholder = {};
	$scope.shareholder.id = "";
	$scope.shareholder.name = "";
	$scope.shareholder.surname = "";
	$scope.shareholder.userName = "";
	$scope.shareholder.password = "";
	$scope.shareholder.role = "";
	$scope.shareholder.projectIds = [];
	
	$scope.projects = [];
// $scope.bugs = [];
	
	var getProjects = function() {
		$http.get(projectsUrl).then(
				function success(res) {
					$scope.projects = res.data;
					getShareHolder();
				},
				function error(res) {
					alert("Couldn't fetch projects!");
				}
		);
	}
	
	var getShareHolder = function() {
		$http.get(shareholderUrl).then(
				function success(res) {
					$scope.shareholder = res.data;
				},
				function error(res) {
					alert("Couldn't fetch shareholder!");
				}
		);
	}

	getProjects();
	
	var newArray = [];
	
	$scope.iterateProjectIds = function(pid) {
		var retVal = false;
		
		if($scope.shareholder.projectIds != null) {
			for(var i = 0, l = $scope.shareholder.projectIds.length; i < l; i++) {

				if(pid == $scope.shareholder.projectIds[i]) {
					retVal = true;
					newArray.push(pid);
				}
			}
		}
		
		return retVal;
	};
	
	$scope.pushSplice = function(pid) {
		
		if($scope.shareholder.projectIds == null) {
			$scope.shareholder.projectIds = [];
			$scope.shareholder.projectIds[0] = pid;
		} else {
			var i = $scope.shareholder.projectIds.indexOf(pid);
			
			if(i != -1) {
				$scope.shareholder.projectIds.splice(i, 1);
			} else {
				$scope.shareholder.projectIds.push(pid);
			}
		}
	}
	
	$scope.doEdit = function() {
		$http.put(shareholderUrl, $scope.shareholder).then(
				function success() {
					$location.path("/shareholders");					
				},
				function error() {
					alert("Something went wrong!");
				}
		);
	}
	
});

btsApp.controller("ProjectsCtrl", function($scope, $http, $location) {
	var url = "/api/projects";
	
	$scope.projects = [];
	
	var getProjects = function() {
		var promise = $http.get(url);
		promise.then(
				function success(res) {
					$scope.projects = res.data;
				},
				function error(res) {
					alert("Couldn't fetch projects!");
				}
		);
	}
	
	getProjects();
	
	$scope.goToAdd = function() {
		$location.path("/projects/add");
	}
	
	$scope.goToEdit = function(id) {
		$location.path("/projects/edit/" + id);
	}
	
	$scope.doDelete = function(id) {
		var promise = $http.delete(url + "/" + id);
		promise.then(
				function success() {
					getProjects();
				},
				function error() {
					alert("Couldn't delete project!");
				}
		);
	}
});

btsApp.controller("AddProjectCtrl", function($scope, $http, $location) {
	var url = "/api/projects";
	
	$scope.project = {};
	$scope.project.name = "";
	$scope.project.projectVersion = "";
	
	$scope.doAdd = function() {
		$http.post(url, $scope.project).then(
				function success() {
					$location.path("/projects");
				},
				function error() {
					alert("Couldn't save the project!");
				}
		);
	}
});

btsApp.controller("EditProjectCtrl", function($scope, $http, $routeParams, $location) {
	var url = "/api/projects/" + $routeParams.pid;
	var shareholderUrl = "/api/shareholders";

	$scope.project = {};
	$scope.project.name = "";
	$scope.project.projectVersion = "";

	var getProject = function() {
		var promise = $http.get(url);
		promise.then(
			function success(odg) {
				$scope.project = odg.data;
			},
			function error(odg) {
				alert("Couldn't fetch project!");
			}
		);
	}

	getProject();

	$scope.doEdit = function() {
		var promise = $http.put(url, $scope.project);
		promise.then(
			function success(odg) {
				$location.path("/projects");
			},
			function error(odg) {
				alert("Couldn't commit changes!");
			}
		);
	}
});

btsApp.controller("BugsCtrl", function($scope, $routeParams, $http, $location) {
	var url = "/api/bugs";
	var projectsUrl = "/api/projects/";
	var shareHoldersUrl = "/api/shareholders/";
	var iterateBugUrl = "/api/bugs/iterate";
	
	
	$scope.bugs = [];
	$scope.projects = [];
	$scope.shareholders = [];

	var statusesUrl = "/api/states/status";
	var severitiesUrl = "/api/states/severity";
	var closedStatusesUrl = "/api/states/closedstatus";
	
	$scope.statuses = [];
	$scope.severities = [];
	$scope.closedStatuses = [];
	
	$scope.newBug = {};
	$scope.newBug.title = "";
	$scope.newBug.description = "";
	$scope.newBug.created = "";
	$scope.newBug.opened = "";
	$scope.newBug.closed = "";
	$scope.newBug.severity = "";
	$scope.newBug.status = "";
	$scope.newBug.closedStatus = "";
	$scope.newBug.createdById = "";
	$scope.newBug.closedById = "";
	$scope.newBug.projectId = "";
	
	//TODO:SEARCH PARAMETERS
	$scope.search = {};
	$scope.search.sTitle = "";
	$scope.search.sStatus = "";
	$scope.search.sSeverity = "";
	$scope.search.sClosedStatus = "";
	
	$scope.pageNum = 0;
	$scope.totalPages = 1;
	
	var getBugs = function() {
		var config = {params:{}};
		
		//TODO:SEARCH PARAMS FILTERING
		if($scope.search.sTitle != "") {
			config.params.title = $scope.search.sTitle;
		}
		if($scope.search.sStatus != "") {
			config.params.status = $scope.search.sStatus;
		}
		if($scope.search.sSeverity != "") {
			config.params.severity = $scope.search.sSeverity;
		}
		if($scope.search.sClosedStatus != "") {
			config.params.closedStatus = $scope.search.sClosedStatus;
		}
		
		config.params.pageNum = $scope.pageNum;
		
		var promise = $http.get(url, config);
		promise.then(
			function success(res) {
				$scope.bugs = res.data;
				$scope.totalPages = res.headers("totalPages");
				getProjects();
				getShareHolders();
			},
			function error() {
				alert("Couldn't fetch bugs!");
			}
		);
	}
	
	var getProjects = function() {
		var promise = $http.get(projectsUrl);
		promise.then(
				function success(res) {
					$scope.projects = res.data;
				},
				function error() {
					alert("Couldn't fetch projects!");
				}
		);
	}
	
	var getShareHolders = function() {
		var promise = $http.get(shareHoldersUrl);
		promise.then(
				function success(res) {
					$scope.shareholders = res.data;
				},
				function error() {
					alert("Couldn't fetch projects!");
				}
		);
	}
	
	getBugs();
	
	//ENUM FETCHING:
	
	var getSeverities = function() {
		var promise = $http.get(severitiesUrl);
		promise.then(
				function success(res) {
					$scope.severities = res.data;
				}, 
				function error(res) {
					alert("Couldn't fetch severities!");
				}
		);
	}
	
	var getStatuses = function() {
		var promise = $http.get(statusesUrl);
		promise.then(
				function success(res) {
					$scope.statuses = res.data;
				}, 
				function error(res) {
					alert("Couldn't fetch statuses!");
				}
		);
	}
	
	var getClosedStatuses = function() {
		var promise = $http.get(closedStatusesUrl);
		promise.then(
				function success(res) {
					$scope.closedStatuses = res.data;
					
				}, 
				function error(res) {
					alert("Couldn't fetch closed statuses!");
				}
		);
	}
	
	getClosedStatuses();
	getSeverities();
	getStatuses();
	
	$scope.doAdd = function() {
		var promise = $http.post(url, $scope.newBug);
		promise.then(
				function success(res) {
					getBugs();
					$scope.newBug = {};
				},
				function error(res) {
					alert("Couldn't add new bug!");
				}
		);
	}
	
	$scope.doSearch = function() {
		getBugs();
	}
	
	$scope.resolve = {};
	$scope.resolve.shareholderId = "";
	$scope.resolve.cStatus = "";
	
	$scope.checkState = function(b) {
		if(b.status == "NEW") {
			$http.put(url + "/iterate", b).then(
					function success(res) {
						getBugs();
					},
					function error() {
						alert("Couldn't iterate bug state!");
					}
			);
		}
		
		if(b.status == "IN_PROGRESS") {
			  $scope.showModalIptc = true;
		}
	}

	$scope.ok = function(b) {
		
		var iterationConfig = {params:{}};
		
		if($scope.resolve.shareholderId != "") {
			iterationConfig.params.shId = $scope.resolve.shareholderId;
		}
		
		if($scope.resolve.cStatus != "") {
			iterationConfig.params.cs = $scope.resolve.cStatus;
		}
				
		$http.put(url + "/iterate", b, iterationConfig).then(
				function success(res) {
					getBugs();
					$scope.showModal = false;
					$scope.resolve = {};
				},
				function error() {
					alert("Couldn't iterate bug state!");
				}
		);
		
//	  $scope.showModal = false;
	};

	$scope.cancel = function() {
	  $scope.showModalIptc = false;
	};
});

btsApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: '/app/html/home.html',
			controller: "HomeCtrl"
		})
		.when("/bugs", {
			templateUrl: '/app/html/bugs.html'
		})
		.when('/login', {
			templateUrl: '/app/html/login.html'
		})
		.when('/projects', {
			templateUrl: '/app/html/projects.html'
		})
		.when('/projects/add', {
			templateUrl: '/app/html/add-project.html'
		})
		.when('/projects/edit/:pid', {
			templateUrl: '/app/html/edit-project.html'
		})
		.when('/shareholders', {
			templateUrl: 'app/html/shareholders.html'
		})
		.when('/shareholders/add', {
			templateUrl: 'app/html/add-shareholder.html'
		})
		.when('/shareholders/edit/:shid', {
			templateUrl: 'app/html/edit-shareholder.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);




















































