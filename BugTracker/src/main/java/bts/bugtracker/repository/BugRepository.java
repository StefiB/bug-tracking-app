package bts.bugtracker.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bts.bugtracker.model.Bug;
import bts.bugtracker.model.ClosedStatus;
import bts.bugtracker.model.Severity;
import bts.bugtracker.model.Status;

@Repository
public interface BugRepository extends JpaRepository<Bug, Long> {

	@Query("SELECT b FROM Bug b WHERE (:shareHolderId IS NULL OR b.closedBy.id = :shareHolderId)")
	List<Bug> findByClosedById(@Param("shareHolderId") Long shareHolderId);

	@Query("SELECT b FROM Bug b WHERE (:shareHolderId IS NULL OR b.createdBy.id = :shareHolderId)")
	List<Bug> findByCreatedById(@Param("shareHolderId") Long shareHolderId);

	@Query("SELECT b FROM Bug b WHERE (:projectId IS NULL OR b.project.id = :projectId)")
	List<Bug> findByProjectId(@Param("projectId") Long projectId);

	@Query("SELECT b FROM Bug b WHERE "
			+ "(:title IS NULL or b.title LIKE %:title%) AND "
			+ "(:status IS NULL or b.status = :status) AND "
			+ "(:severity IS NULL or b.severity = :severity) AND "
			+ "(:closedStatus IS NULL OR b.closedStatus = :closedStatus)"
			)
	Page<Bug> search(
			@Param("title") String title, 
			@Param("status")Status status,
			@Param("severity")Severity severity,
			@Param("closedStatus")ClosedStatus closedStatus,
			Pageable pageNum
			);

}
