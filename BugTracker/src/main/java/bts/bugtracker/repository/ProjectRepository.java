package bts.bugtracker.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bts.bugtracker.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

	@Query("SELECT p FROM Project p JOIN p.shareHolders sh WHERE (sh.id = :shareHolderId)")
	List<Project> findByShareHolderId(@Param("shareHolderId") Long shareHolderId);

//	@Query("DELETE p FROM Project p JOIN ShareHolder sh WHERE (sh.id = :shareHolderId)")
//	void deleteByShareHolderId(@Param("shareHolderId") Long shareHolderId);
	
	
	/*
	 * DELETE t1
FROM table_name1 AS t1 JOIN {INNER, RIGHT,LEFT,FULL} table_name1 AS t2
ON t1.column_name = t2.column_name
WHERE condition;
	 */
}
