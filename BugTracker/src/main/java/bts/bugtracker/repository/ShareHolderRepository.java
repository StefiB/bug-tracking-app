package bts.bugtracker.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import bts.bugtracker.model.ShareHolder;

@Repository
public interface ShareHolderRepository extends JpaRepository<ShareHolder, Long> {

	@Query("SELECT sh FROM ShareHolder sh JOIN sh.projects p WHERE (p.id = :projectId)")
	List<ShareHolder> findByProjectId(@Param("projectId") Long projectId);
	
	@Modifying
	@Transactional
	@Query(value = "DELETE FROM tbl_project_participants WHERE "
			+ "project_id = :projectId AND share_holder_id = :shareholderId",
			nativeQuery = true)
	void removeByProjectIdAndShareHolderId(
			@Param("projectId") Long projectId,
			@Param("shareholderId") Long shareholderId);
	
	Optional<ShareHolder> findByUserName(String userName);
}
