package bts.bugtracker.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import bts.bugtracker.model.Bug;
import bts.bugtracker.model.ClosedStatus;
import bts.bugtracker.model.Severity;
import bts.bugtracker.model.Status;

public interface BugService {

	Bug findOne(Long id);
	List<Bug> findAll();
	Page<Bug> findAll(int PageNum);
	Bug save(Bug bug);
	Bug delete(Long id);
	List<Bug> findByClosedBy(Long shareHolderId);
	List<Bug> findByCreatedBy(Long id);
	List<Bug> findAllProjectsBugs(Long id);
	Page<Bug> search(
			@Param(value = "title") String title, 
			@Param(value = "status") Status status,
			@Param(value = "severity") Severity severity,
			@Param(value = "closedStatus") ClosedStatus closedStatus,
			int pageNum);
}
