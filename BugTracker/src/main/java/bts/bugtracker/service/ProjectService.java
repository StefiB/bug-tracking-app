package bts.bugtracker.service;

import java.util.List;

import bts.bugtracker.model.Project;

public interface ProjectService {

	Project findOne(Long id);
	List<Project> findAll();
	Project save(Project project);
	Project delete(Long id);
	List<Project> findByShareHolderId(Long id);
}
