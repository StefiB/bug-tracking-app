package bts.bugtracker.service;

import java.util.List;

import bts.bugtracker.model.Status;

public interface StatusService {

	List<Status> findAll();
	Status findOne(int id);

}
