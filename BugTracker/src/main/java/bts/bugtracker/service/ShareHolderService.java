package bts.bugtracker.service;

import java.util.List;

import bts.bugtracker.model.ShareHolder;

public interface ShareHolderService {

	ShareHolder findOne(Long id);
	List<ShareHolder> findAll();
	ShareHolder save(ShareHolder shareHolder);
	ShareHolder delete(Long id);
	List<ShareHolder> findByProjectId(Long id);
	void removeProjectByShId(Long projId, Long shareholderId);
}
