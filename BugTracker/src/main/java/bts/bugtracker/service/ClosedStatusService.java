package bts.bugtracker.service;

import java.util.List;

import bts.bugtracker.model.ClosedStatus;

public interface ClosedStatusService {

	ClosedStatus findOne(int id);
	List<ClosedStatus> findAll();
}
