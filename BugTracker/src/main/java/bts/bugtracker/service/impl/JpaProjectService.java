package bts.bugtracker.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bts.bugtracker.model.Project;
import bts.bugtracker.repository.ProjectRepository;
import bts.bugtracker.service.ProjectService;

@Service
public class JpaProjectService implements ProjectService {

	@Autowired
	private ProjectRepository projRepo;
	
	@Override
	public Project findOne(Long id) {
		Project retVal = null;
		Optional<Project> prj = projRepo.findById(id);
		
		if(!prj.isPresent()) {
			throw new IllegalArgumentException("Id not found!");
		}
		
		retVal = prj.get();
		
		return retVal;
	}

	@Override
	public List<Project> findAll() {
		return projRepo.findAll();
	}

	@Override
	public Project save(Project project) {
		return projRepo.save(project);
	}

	@Override
	public Project delete(Long id) {
		Project retVal = null;
		Optional<Project> prj = projRepo.findById(id);
		
		if(!prj.isPresent()) {
			throw new IllegalArgumentException("Tried to delete non-existant entry");
		}
		
		retVal = prj.get();
		projRepo.deleteById(id);
		
		return retVal;
	}

	@Override
	public List<Project> findByShareHolderId(Long id) {
		return projRepo.findByShareHolderId(id);
	}

}
