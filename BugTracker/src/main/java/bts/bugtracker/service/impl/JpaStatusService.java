package bts.bugtracker.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import bts.bugtracker.model.Status;
import bts.bugtracker.service.StatusService;

@Service
public class JpaStatusService implements StatusService {

	@Override
	public List<Status> findAll() {
		return Status.getOrderedValues();
	}

	@Override
	public Status findOne(int index) {
		return Status.values()[index];
	}

}
