package bts.bugtracker.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import bts.bugtracker.model.Severity;
import bts.bugtracker.service.SeverityService;

@Service
public class JpaSeverityService implements SeverityService{
	
	@Override
	public Severity findOne(int index) {
		return Severity.values()[index];
	}

	@Override
	public List<Severity> findAll() {
		return Severity.getOrderedValues();
	}

}
