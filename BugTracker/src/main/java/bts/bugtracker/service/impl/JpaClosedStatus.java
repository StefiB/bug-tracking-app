package bts.bugtracker.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import bts.bugtracker.model.ClosedStatus;
import bts.bugtracker.service.ClosedStatusService;

@Service
public class JpaClosedStatus implements ClosedStatusService {

	@Override
	public ClosedStatus findOne(int index) {
		return ClosedStatus.values()[index];
	}

	@Override
	public List<ClosedStatus> findAll() {
		return ClosedStatus.getOrderedValues();
	}

}
