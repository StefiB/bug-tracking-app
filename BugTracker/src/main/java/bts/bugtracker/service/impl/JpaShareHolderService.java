package bts.bugtracker.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import bts.bugtracker.SecurityConfiguration;
import bts.bugtracker.model.ShareHolder;
import bts.bugtracker.model.ShareholdersDetails;
import bts.bugtracker.repository.ShareHolderRepository;
import bts.bugtracker.service.ShareHolderService;

@Service
public class JpaShareHolderService implements ShareHolderService, UserDetailsService {

	@Autowired
	private ShareHolderRepository shareHolderRepo;
	
	@Autowired
	private PasswordEncoder passEncode;
	
	@Override
	public ShareHolder findOne(Long id) {
		ShareHolder retVal = null;
		Optional<ShareHolder> shareHolder = shareHolderRepo.findById(id);
		
		if(!shareHolder.isPresent()) {
			throw new IllegalArgumentException("Tried to fetch non-existant entry!");
		}
		
		retVal = shareHolder.get();
		
		return retVal;
	}

	@Override
	public List<ShareHolder> findAll() {
		
		List<ShareHolder> shareHolders = shareHolderRepo.findAll();
		
		if(shareHolders.isEmpty()) {
			throw new NullPointerException("Empty register!");
		}
		
		return shareHolders;
	}

	@Override
	public ShareHolder save(ShareHolder shareHolder) {
		String pass = shareHolder.getPassword();
		shareHolder.setPassword(new BCryptPasswordEncoder().encode(pass));
		return shareHolderRepo.save(shareHolder);
	}

	@Override
	public ShareHolder delete(Long id) {
		ShareHolder retVal = null;
		Optional<ShareHolder> shareHolder = shareHolderRepo.findById(id);
		
		if(!shareHolder.isPresent()) {
			throw new IllegalArgumentException("Tried to delete non-existant entry!");
		}
		
		retVal = shareHolder.get();
		shareHolderRepo.delete(retVal);
		
		return retVal;
	}

	@Override
	public List<ShareHolder> findByProjectId(Long id) {
		return shareHolderRepo.findByProjectId(id);
	}

	@Override
	public void removeProjectByShId(Long projId, Long shareholderId) {
		shareHolderRepo.removeByProjectIdAndShareHolderId(projId, shareholderId);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		ShareHolder retVal = shareHolderRepo.findByUserName(username).get();
		
		if(retVal == null) {
			throw new IllegalArgumentException("User with " + username + " not found!");
		}
		
		return new ShareholdersDetails(retVal);
	}
}


















