package bts.bugtracker.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import bts.bugtracker.model.Bug;
import bts.bugtracker.model.ClosedStatus;
import bts.bugtracker.model.Severity;
import bts.bugtracker.model.Status;
import bts.bugtracker.repository.BugRepository;
import bts.bugtracker.service.BugService;

@Service
public class JpaBugService implements BugService {

	@Autowired
	private BugRepository bugRepo;
	
	@Override
	public Bug findOne(Long id) {
		Bug retVal = null;
		Optional<Bug> bug = bugRepo.findById(id);
		
		if(!bug.isPresent()) {
			throw new IllegalArgumentException("Id not found");
		}
		
		retVal = bug.get();
		
		return retVal;
	}

	@Override
	public List<Bug> findAll() {
		return bugRepo.findAll();
	}

	@Override
	public Page<Bug> findAll(int PageNum) {
		return bugRepo.findAll(PageRequest.of(PageNum, 10));
	}

	@Override
	public Bug save(Bug bug) {
		return bugRepo.save(bug);
	}

	@Override
	public Bug delete(Long id) {
		Bug retVal = null;
		Optional<Bug> bug = bugRepo.findById(id);
		
		if(!bug.isPresent()) {
			throw new IllegalArgumentException("Tried to delete a non-existant entry");
		}
			
		retVal = bug.get();		
		bugRepo.deleteById(id);
		
		return retVal;
	}

	@Override
	public List<Bug> findByClosedBy(Long shareHolderId) {
		return bugRepo.findByClosedById(shareHolderId);
	}

	@Override
	public List<Bug> findByCreatedBy(Long shareHolderId) {
		return bugRepo.findByCreatedById(shareHolderId);
	}

	@Override
	public List<Bug> findAllProjectsBugs(Long id) {
		return bugRepo.findByProjectId(id);
	}

	@Override
	public Page<Bug> search(
			String title,
			Status status,
			Severity severity,
			ClosedStatus closedStatus,
			int pageNum
			) {
		return bugRepo.search(title, status, severity, closedStatus, PageRequest.of(pageNum, 10));
	}

}
