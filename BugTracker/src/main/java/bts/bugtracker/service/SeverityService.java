package bts.bugtracker.service;

import java.util.List;

import bts.bugtracker.model.Severity;

public interface SeverityService {

	Severity findOne(int id);
	List<Severity> findAll();
}
