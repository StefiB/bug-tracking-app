package bts.bugtracker.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bts.bugtracker.model.Project;
import bts.bugtracker.service.ProjectService;
import bts.bugtracker.web.dto.ProjectDTO;

@Component
public class ProjectDtoToProject implements Converter<ProjectDTO, Project> {

	@Autowired
	private ProjectService projServ;
	
	@Override
	public Project convert(ProjectDTO dto) {
		
		Project project = null;
		
		if(dto.getId() != null) {
			project = projServ.findOne(dto.getId());
		} else {
			project = new Project();
		}
		
		project.setId(dto.getId());
		project.setName(dto.getName());
		project.setProjectVersion(dto.getProjectVersion());
		
		return project;
	}

	public List<Project> convert(List<ProjectDTO> dtos) {
		List<Project> projs = new ArrayList<>();
		
		for(ProjectDTO dto : dtos) {
			projs.add(convert(dto));
		}
		
		return projs;
	}
}
