package bts.bugtracker.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bts.bugtracker.model.Project;
import bts.bugtracker.model.ShareHolder;
import bts.bugtracker.web.dto.ShareHolderDTO;

@Component
public class ShareHolderToShareHolderDto implements Converter<ShareHolder, ShareHolderDTO>{

	@Override
	public ShareHolderDTO convert(ShareHolder src) {
		
		ShareHolderDTO dto = new ShareHolderDTO();
		
		dto.setId(src.getId());
		dto.setName(src.getName());
		dto.setSurname(src.getSurname());
		dto.setUserName(src.getUserName());
		dto.setRole(src.getRole());
		
		if(!src.getProjects().isEmpty()) {
			
			List<Long> projectIds = new ArrayList<>();
			
			for(Project p : src.getProjects()) {
				projectIds.add(p.getId());
			}
			
			dto.setProjectIds(projectIds);
		}
		
		return dto;
	}

	public List<ShareHolderDTO> convert(List<ShareHolder> srcs) {
		List<ShareHolderDTO> dtos = new ArrayList<ShareHolderDTO>();
		
		for(ShareHolder sh : srcs) {
			dtos.add(convert(sh));
		}
		
		return dtos;
	}
}
