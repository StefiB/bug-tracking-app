package bts.bugtracker.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bts.bugtracker.model.Bug;
import bts.bugtracker.model.Status;
import bts.bugtracker.web.dto.BugDTO;

@Component
public class BugToBugDto implements Converter<Bug, BugDTO>{

	
	@Override
	public BugDTO convert(Bug src) {
		
		BugDTO dto = new BugDTO();
		
		if(src.getStatus() == null) {
			src.setStatus(Status.IN_PROGRESS);
		}
		
		if(src.getStatus().toString().equalsIgnoreCase("CLOSED")) {
			dto.setId(src.getId());
			dto.setTitle(src.getTitle());				
			dto.setDescription(src.getDescription());				
			dto.setProjectId(src.getProject().getId());				

			dto.setOpened(src.getOpened());				
			dto.setCreated(src.getCreated());				
			dto.setClosed(src.getClosed());				

			dto.setCreatedById(src.getCreatedBy().getId());				
			dto.setClosedById(src.getClosedBy().getId());				
			
			dto.setClosedStatus(src.getClosedStatus().toString());				
			dto.setSeverity(src.getSeverity().toString());				
			dto.setStatus(src.getStatus().toString());
			
		} else if(!src.getStatus().toString().equalsIgnoreCase("CLOSED")) {
			
			dto.setId(src.getId());
			dto.setTitle(src.getTitle());
			dto.setDescription(src.getDescription());
			dto.setProjectId(src.getProject().getId());

			dto.setOpened(src.getOpened());
			dto.setCreated(src.getCreated());

			dto.setCreatedById(src.getCreatedBy().getId());
			
			dto.setSeverity(src.getSeverity().toString());
			dto.setStatus(src.getStatus().toString());
		}
		
		return dto;
	}

	public List<BugDTO> convert(List<Bug> bugs) {
		
		List<BugDTO> dtos = new ArrayList<>();
		
		for(Bug bug : bugs) {
			dtos.add(convert(bug));
		}
		
		return dtos;
	}
}
