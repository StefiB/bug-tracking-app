package bts.bugtracker.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bts.bugtracker.model.Project;
import bts.bugtracker.model.ShareHolder;
import bts.bugtracker.service.ProjectService;
import bts.bugtracker.service.ShareHolderService;
import bts.bugtracker.web.dto.ShareHolderDTO;

@Component
public class ShareHolderDtoToShareHolder implements Converter<ShareHolderDTO, ShareHolder>{

	@Autowired
	private ShareHolderService shHoldServ;
	
	@Autowired
	private ProjectService projServ;
	
	@Override
	public ShareHolder convert(ShareHolderDTO dto) {
		
		ShareHolder sh = null;
		
		if(dto.getId() != null) {
			sh = shHoldServ.findOne(dto.getId());
		} else {
			sh = new ShareHolder();
		}
		
		System.out.println("From toShareHolder converter:");
		sh.setId(dto.getId());
		System.out.println(dto.getId());
		sh.setName(dto.getName());
		System.out.println(dto.getName());
		sh.setSurname(dto.getSurname());
		System.out.println(dto.getSurname());
		sh.setUserName(dto.getUserName());
		System.out.println(dto.getUserName());
		sh.setPassword(dto.getPassword());
		System.out.println(dto.getPassword());
		sh.setRole(dto.getRole());
		System.out.println(dto.getRole());
		
		if(!dto.getProjectIds().isEmpty()) {
			for(Long pId : dto.getProjectIds()) {
				sh.addProject(projServ.findOne(pId));
				System.out.println(pId);
			}
		}
		
		return sh;
	}

	public List<ShareHolder> convert(List<ShareHolderDTO> dtos) {
		List<ShareHolder> shs = new ArrayList<ShareHolder>();
		
		for(ShareHolderDTO dto : dtos) {
			shs.add(convert(dto));
		}
		
		return shs;
	}
}
