package bts.bugtracker.support;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bts.bugtracker.model.Bug;
import bts.bugtracker.model.ClosedStatus;
import bts.bugtracker.model.Project;
import bts.bugtracker.model.Severity;
import bts.bugtracker.model.ShareHolder;
import bts.bugtracker.model.Status;
import bts.bugtracker.service.BugService;
import bts.bugtracker.service.ProjectService;
import bts.bugtracker.service.ShareHolderService;
import bts.bugtracker.web.dto.BugDTO;

@Component
public class BugDtoToBug implements Converter<BugDTO, Bug> {

	@Autowired
	private BugService bugServ;
	
	@Autowired
	private ShareHolderService sHoldServ;
	
	@Autowired
	private ProjectService projServ;
	
	@Override
	public Bug convert(BugDTO dto) {
		
		ShareHolder createdBy = sHoldServ.findOne(dto.getCreatedById());
		Project project = projServ.findOne(dto.getProjectId());
		ShareHolder closedBy = null;
		
		if(dto.getClosedById() != null) {
			closedBy = sHoldServ.findOne(dto.getClosedById());
		}
		
		if(createdBy != null && project != null) {
			Bug bug = null;
			
			if(dto.getId() != null) {
				bug = bugServ.findOne(dto.getId());
			} else {
				bug = new Bug();
			}
			
			bug.setTitle(dto.getTitle());
			bug.setDescription(dto.getDescription());
			
			bug.setStatus(Status.NEW);
			bug.setCreated(Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")).toString()));
			bug.setOpened(dto.getOpened());
			bug.setCreatedBy(createdBy);
			
			bug.setSeverity(Severity.valueOf(dto.getSeverity()));
			bug.setProject(project);
			
			if(closedBy != null) {
				bug.setClosed(dto.getClosed());
				bug.setClosedBy(closedBy);
				bug.setClosedStatus(ClosedStatus.valueOf(dto.getClosedStatus()));
			} 
			
			return bug;
		} else {
			throw new IllegalStateException("Trying to attach to non-existing entities");
		}
		
	}

	public List<Bug> convert(List<BugDTO> dtos) {
		List<Bug> bugs = new ArrayList<Bug>();
		
		for(BugDTO dto : dtos) {
			bugs.add(convert(dto));
		}
		
		return bugs;
	}
}
