package bts.bugtracker.support.enum_converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import bts.bugtracker.model.ClosedStatus;

@Converter(autoApply = true)
public class ClosedStatusConverter implements AttributeConverter<ClosedStatus, String> {

	@Override
	public String convertToDatabaseColumn(ClosedStatus closedStatus) {
		if(closedStatus == null) {
			return null;
		}
		
		return closedStatus.getClosedStatus();
	}

	@Override
	public ClosedStatus convertToEntityAttribute(String dbClosedStatus) {

		if(dbClosedStatus == null) {
			return null;
		}
		
		return ClosedStatus.fromValue(dbClosedStatus);
	}
	
}
