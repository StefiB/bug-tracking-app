package bts.bugtracker.support.enum_converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import bts.bugtracker.model.Severity;

@Converter(autoApply = true)
public class SeverityConverter implements AttributeConverter<Severity, String> {

	@Override
	public String convertToDatabaseColumn(Severity severity) {
		
		if(severity == null) {
			return null;
		}
		
		return severity.getSeverity();
	}

	@Override
	public Severity convertToEntityAttribute(String dbSeverity) {
		
		if(dbSeverity == null) {
			return null;
		}
		
		return Severity.fromValue(dbSeverity);
	}
	
}
