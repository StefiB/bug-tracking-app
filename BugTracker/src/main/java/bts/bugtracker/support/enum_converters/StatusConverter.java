package bts.bugtracker.support.enum_converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import bts.bugtracker.model.Status;

@Converter(autoApply = true)
public class StatusConverter implements AttributeConverter<Status, String> {

	@Override
	public String convertToDatabaseColumn(Status status) {

		if(status == null) {
			return null;
		}
		
		return status.getStatus();
	}

	@Override
	public Status convertToEntityAttribute(String dbStatus) {
		
		if(dbStatus == null) {
			return null;
		}
		
		return Status.fromValue(dbStatus);
	}
	
}