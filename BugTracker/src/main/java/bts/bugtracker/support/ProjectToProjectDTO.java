package bts.bugtracker.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bts.bugtracker.model.Project;
import bts.bugtracker.web.dto.ProjectDTO;

@Component
public class ProjectToProjectDTO implements Converter<Project, ProjectDTO>{

	@Override
	public ProjectDTO convert(Project src) {
		
		ProjectDTO dto = new ProjectDTO();
		
		dto.setId(src.getId());
		dto.setName(src.getName());
		dto.setProjectVersion(src.getProjectVersion());
		
		return dto;
	}

	public List<ProjectDTO> convert(List<Project> projs) {
		List<ProjectDTO> dtos = new ArrayList<ProjectDTO>();
		
		for(Project p : projs) {
			dtos.add(convert(p));
		}
		
		return dtos;
	}
}
