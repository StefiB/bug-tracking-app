package bts.bugtracker.web.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsAsyncClientHttpRequestFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bts.bugtracker.model.Bug;
import bts.bugtracker.model.ClosedStatus;
import bts.bugtracker.model.Severity;
import bts.bugtracker.model.Status;
import bts.bugtracker.service.BugService;
import bts.bugtracker.service.ShareHolderService;
import bts.bugtracker.support.BugDtoToBug;
import bts.bugtracker.support.BugToBugDto;
import bts.bugtracker.web.dto.BugDTO;
import bts.bugtracker.web.dto.ResolveBugDTO;

@RestController
@RequestMapping(value = "/api/bugs")
public class ApiBugController {

	@Autowired
	private BugService bugServ;
	
	@Autowired
	private BugToBugDto toDto;
	
	@Autowired
	private BugDtoToBug toBug;

	@Autowired
	private ShareHolderService shareHolderService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<BugDTO>> getBugs(
			@RequestParam(value = "pageNum", defaultValue = "0") int pageNum,
			@RequestParam(required = false) String title,
			@RequestParam(required = false) String status,
			@RequestParam(required = false) String severity,
			@RequestParam(required = false) String closed_status) {
		
		Page<Bug> bugPage = null;
		
		if(title != null || status != null || severity != null || closed_status != null) {
			
			bugPage = bugServ.search(title,
					Status.fromValue(status),
					Severity.fromValue(severity),
					ClosedStatus.fromValue(closed_status),
					pageNum);
			
		} else {
			bugPage = bugServ.findAll(pageNum);
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(bugPage.getTotalPages()));
		
		return new ResponseEntity<List<BugDTO>>(
				toDto.convert(bugPage.getContent()),
						headers,
						HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<BugDTO> getBug(@PathVariable Long id) {
		
		Bug bug = bugServ.findOne(id);
		
		if(bug == null) {
			return new ResponseEntity<BugDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<BugDTO>(toDto.convert(bug), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<BugDTO> delete(@PathVariable Long id) {
		
		Bug deleted = bugServ.delete(id);
		
		if(deleted == null) {
			return new ResponseEntity<BugDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<BugDTO>(toDto.convert(deleted), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<BugDTO> add(@Validated @RequestBody BugDTO newBugDto) {
		
		Bug savedBug = bugServ.save(toBug.convert(newBugDto));
		
		return new ResponseEntity<BugDTO>(toDto.convert(savedBug), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.PUT,
			value = "/{id}",
			consumes = "application/json")
	public ResponseEntity<BugDTO> edit(
			@Validated @RequestBody BugDTO bugDto,
			@PathVariable Long id) {
		
		if(!id.equals(bugDto.getId())) {
			return new ResponseEntity<BugDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Bug persisted = bugServ.save(toBug.convert(bugDto));
		
		return new ResponseEntity<BugDTO>(toDto.convert(persisted), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT,
			value = "/iterate",
			consumes = "application/json")
	public ResponseEntity<BugDTO> nextPhase(
			@RequestBody BugDTO bugToIterate,
			@RequestParam(required = false) Long shId,
			@RequestParam(required = false) String cs) {
		
		System.out.println(shId);
		System.out.println(cs);

		Bug toIterate = bugServ.findOne(bugToIterate.getId());
		
		if(shId == null && cs == null) {
			toIterate.setStatus(toIterate.getStatus().nextStatus());
			toIterate.setOpened(Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")).toString()));
		} else {
			toIterate.setStatus(toIterate.getStatus().nextStatus());
			toIterate.setClosed(Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")).toString()));
			toIterate.setClosedBy(shareHolderService.findOne(shId));
			toIterate.setClosedStatus(ClosedStatus.fromValue(cs));
		}
		
		bugServ.save(toIterate);
		
		return new ResponseEntity<BugDTO>(toDto.convert(toIterate), HttpStatus.OK);
		
	}
}



















