package bts.bugtracker.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bts.bugtracker.model.Bug;
import bts.bugtracker.model.Project;
import bts.bugtracker.model.ShareHolder;
import bts.bugtracker.service.BugService;
import bts.bugtracker.service.ProjectService;
import bts.bugtracker.service.ShareHolderService;
import bts.bugtracker.support.BugToBugDto;
import bts.bugtracker.support.ProjectToProjectDTO;
import bts.bugtracker.support.ShareHolderDtoToShareHolder;
import bts.bugtracker.support.ShareHolderToShareHolderDto;
import bts.bugtracker.web.dto.BugDTO;
import bts.bugtracker.web.dto.ProjectDTO;
import bts.bugtracker.web.dto.ShareHolderDTO;

@RestController
@RequestMapping(value = "/api/shareholders")
public class ApiShareHolderController {

	@Autowired
	private ShareHolderService shareHoldServ;
	
	@Autowired
	private ShareHolderDtoToShareHolder toShareHold;
	
	@Autowired
	private ShareHolderToShareHolderDto toDto;
	
	@Autowired
	private BugService bugServ;
	
	@Autowired
	private BugToBugDto bugToDto;
	
	@Autowired
	private ProjectService projServ;
	
	@Autowired
	private ProjectToProjectDTO projToProjDto;
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/projects")
	public ResponseEntity<List<ProjectDTO>> getSHsProjects(@PathVariable Long id) {
		
		List<Project> shareHoldersProjs = projServ.findByShareHolderId(id);
		
		if(shareHoldersProjs.isEmpty())
			return new ResponseEntity<List<ProjectDTO>>(HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<List<ProjectDTO>>(projToProjDto.convert(shareHoldersProjs), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/opened_bugs")
	public ResponseEntity<List<BugDTO>> getSHsOpenedBugs(@PathVariable Long id) {
		
		List<Bug> openedByThis = bugServ.findByCreatedBy(id);
		
		if(openedByThis.isEmpty())
			return new ResponseEntity<List<BugDTO>>(HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<List<BugDTO>>(bugToDto.convert(openedByThis), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/closed_bugs")
	public ResponseEntity<List<BugDTO>> getSHsClosedBugs(@PathVariable Long id) {
		
		List<Bug> closedByThis = bugServ.findByClosedBy(id);
		
		if(closedByThis.isEmpty())
			return new ResponseEntity<List<BugDTO>>(HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<List<BugDTO>>(bugToDto.convert(closedByThis), HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ShareHolderDTO>> getShareHolders() {
		
		List<ShareHolder> allShareHolders = shareHoldServ.findAll();
		
		if(allShareHolders == null)
			return new ResponseEntity<List<ShareHolderDTO>>(HttpStatus.NOT_FOUND);
			
		return new ResponseEntity<List<ShareHolderDTO>>(toDto.convert(allShareHolders), HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<ShareHolderDTO> getShareHolder(@PathVariable Long id) {
		
		ShareHolder sh = null;
		
		try {
			
			sh = shareHoldServ.findOne(id);
			return new ResponseEntity<ShareHolderDTO>(toDto.convert(sh), HttpStatus.OK);
			
		} catch(IllegalArgumentException | NullPointerException e) {
			e.printStackTrace();
			return new ResponseEntity<ShareHolderDTO>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST,
			consumes = "application/json")
	public ResponseEntity<ShareHolderDTO> save(
			@Validated @RequestBody ShareHolderDTO newShareHolder) {
		
		ShareHolder toSave = shareHoldServ.save(toShareHold.convert(newShareHolder));
		
		return new ResponseEntity<ShareHolderDTO>(toDto.convert(toSave), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.PUT,
			value = "/{id}",
			consumes = "application/json")
	public ResponseEntity<ShareHolderDTO> edit(
			@Validated @RequestBody ShareHolderDTO changedShareHolder,
			@PathVariable Long id) {
		
		if(!changedShareHolder.getId().equals(id)) {
			return new ResponseEntity<ShareHolderDTO>(HttpStatus.BAD_REQUEST);
		}
		
		ShareHolder toCompare = shareHoldServ.findOne(id);
		
		for(Project p : toCompare.getProjects()) {
			if(!changedShareHolder.getProjectIds().contains(p.getId())) {
				shareHoldServ.removeProjectByShId(p.getId(), id);
			}
		}
		
		try {
			if(changedShareHolder.getPassword() == null || changedShareHolder.getPassword().equals("")) {
				changedShareHolder.setPassword(toCompare.getPassword());
			}
		} catch(NullPointerException npe) {
			changedShareHolder.setPassword(toCompare.getPassword());
			npe.printStackTrace();
		}
		
		ShareHolder changed = shareHoldServ.save(toShareHold.convert(changedShareHolder));
		
		return new ResponseEntity<ShareHolderDTO>(toDto.convert(changed), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<ShareHolderDTO> delete(@PathVariable Long id) {
		
		ShareHolder toDelete = shareHoldServ.delete(id);
		
		if(toDelete == null) {
			return new ResponseEntity<ShareHolderDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<ShareHolderDTO>(toDto.convert(toDelete), HttpStatus.OK);
	}
}
























