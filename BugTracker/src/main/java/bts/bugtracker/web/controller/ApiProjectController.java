package bts.bugtracker.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bts.bugtracker.model.Bug;
import bts.bugtracker.model.Project;
import bts.bugtracker.model.ShareHolder;
import bts.bugtracker.service.BugService;
import bts.bugtracker.service.ProjectService;
import bts.bugtracker.service.ShareHolderService;
import bts.bugtracker.support.BugToBugDto;
import bts.bugtracker.support.ProjectDtoToProject;
import bts.bugtracker.support.ProjectToProjectDTO;
import bts.bugtracker.support.ShareHolderToShareHolderDto;
import bts.bugtracker.web.dto.BugDTO;
import bts.bugtracker.web.dto.ProjectDTO;
import bts.bugtracker.web.dto.ShareHolderDTO;

@RestController
@RequestMapping(value = "/api/projects")
public class ApiProjectController {

	@Autowired
	private ProjectService projServ;
	
	@Autowired
	private ProjectToProjectDTO toDto;
	
	@Autowired
	private ProjectDtoToProject toProj;
	
	@Autowired
	private BugService bugServ;
	
	@Autowired
	private BugToBugDto bugToBugDto;
	
	@Autowired
	private ShareHolderService shHolderServ;
	
	@Autowired
	private ShareHolderToShareHolderDto shToShDto;
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/shareholders")
	public ResponseEntity<List<ShareHolderDTO>> getAllProjectsShareHolders(@PathVariable Long id) {
		
		List<ShareHolder> allProjectsShareHolders = shHolderServ.findByProjectId(id);
		
		if(allProjectsShareHolders.isEmpty())
			return new ResponseEntity<List<ShareHolderDTO>>(HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<List<ShareHolderDTO>>(shToShDto.convert(allProjectsShareHolders), HttpStatus.OK);
	}
	
	/*
	 * Add methods for retrieving active and resolved bugs (requires enum elaboration)
	 */
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/allbugs")
	public ResponseEntity<List<BugDTO>> getAllProjectsBugs(@PathVariable Long id) {
		
		List<Bug> allProjectsBugs = bugServ.findAllProjectsBugs(id);
		
		if(allProjectsBugs.isEmpty())
			return new ResponseEntity<List<BugDTO>>(HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<List<BugDTO>>(bugToBugDto.convert(allProjectsBugs), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ProjectDTO>> getProjects() {
		
		List<Project> projects = projServ.findAll();
		
		return new ResponseEntity<List<ProjectDTO>>(toDto.convert(projects), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<ProjectDTO> getProject(@PathVariable Long id) {
		
		Project project = projServ.findOne(id);
		
		if(project == null) {
			return new ResponseEntity<ProjectDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<ProjectDTO>(toDto.convert(project), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST,
			consumes = "application/json")
	public ResponseEntity<ProjectDTO> add(
			@Validated @RequestBody ProjectDTO newProj) {
		
		Project toSave = projServ.save(toProj.convert(newProj));
		
		return new ResponseEntity<ProjectDTO>(toDto.convert(toSave), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.PUT,
			value = "/{id}",
			consumes = "application/json")
	public ResponseEntity<ProjectDTO> edit(
			@Validated @RequestBody ProjectDTO changedProject, 
			@PathVariable Long id) {
		
		if(!changedProject.getId().equals(id)) {
			return new ResponseEntity<ProjectDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Project persisted = projServ.save(toProj.convert(changedProject));
		
		return new ResponseEntity<ProjectDTO>(toDto.convert(persisted), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<ProjectDTO> delete(@PathVariable Long id) {
		
		Project toDelete = projServ.delete(id);
		
		if(toDelete == null) {
			return new ResponseEntity<ProjectDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<ProjectDTO>(toDto.convert(toDelete), HttpStatus.OK);
	}
}























