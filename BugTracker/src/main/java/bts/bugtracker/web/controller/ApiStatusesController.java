package bts.bugtracker.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bts.bugtracker.service.ClosedStatusService;
import bts.bugtracker.service.SeverityService;
import bts.bugtracker.service.StatusService;

@RestController
@RequestMapping(value = "/api/states")
public class ApiStatusesController {

	@Autowired
	private ClosedStatusService closedStatusServ;
	
	@Autowired
	private SeverityService severityServ;
	
	@Autowired
	private StatusService statusServ;
	
	@RequestMapping(method = RequestMethod.GET, value = "/{state}")
	public ResponseEntity<List<?>> getStates(@PathVariable(value = "state") String state) {
		List<?> retVal = null;
		
		switch (state) {
			case "closedstatus":
				retVal = closedStatusServ.findAll();
				break;
			case "severity":
				retVal = severityServ.findAll();
				break;
			case "status":
				retVal = statusServ.findAll();
				break;
	
			default:
				break;
		}
		
		if(retVal.isEmpty())
			return new ResponseEntity<List<?>>(HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<List<?>>(retVal, HttpStatus.OK);
	}
}
