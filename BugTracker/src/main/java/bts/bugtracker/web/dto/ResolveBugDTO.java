package bts.bugtracker.web.dto;

public class ResolveBugDTO {

	private Long shareholderId;
	private String closedStatus;
	
	public ResolveBugDTO() {
		super();
	}
	
	public Long getShareholderId() {
		return shareholderId;
	}
	public void setShareholderId(Long shareholderId) {
		this.shareholderId = shareholderId;
	}
	public String getClosedStatus() {
		return closedStatus;
	}
	public void setClosedStatus(String closedStatus) {
		this.closedStatus = closedStatus;
	}
	
	
}
