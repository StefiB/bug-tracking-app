package bts.bugtracker.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

public enum ClosedStatus {

	BY_DESIGN("by design"), FIXED("fixed"), UNDEFINED("undefined");
	
	private String closedStatus;
	
	private ClosedStatus(String closedStatus) {
		this.closedStatus = closedStatus;
	}
	
	
	public static List<ClosedStatus> orderedValues = new ArrayList<>();
	
	static {
		orderedValues.addAll(Arrays.asList(ClosedStatus.values()));
	}
	
	
	public static ClosedStatus fromValue(String value) {
		for(ClosedStatus cs : values()) {
			if(cs.closedStatus.equalsIgnoreCase(value)) {
				return cs;
			} else if(cs.closedStatus == null) {
				return UNDEFINED;
			}
		}
		return null;
		
//		throw new IllegalArgumentException(
//					"Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values())
//				);
	}


	public String getClosedStatus() {
		return closedStatus;
	}
	
	public static List<ClosedStatus> getOrderedValues() {
		List<ClosedStatus> mod = new ArrayList<>(orderedValues);
		mod.remove(mod.size() - 1);
		return mod;
	}
}













