package bts.bugtracker.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity @Table(name = "tbl_project")
public class Project {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String projectVersion;
	
	@OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Bug> bugs = new ArrayList<>();
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinTable(name = "tbl_project_participants",
			joinColumns = @JoinColumn(name = "project_id"),
			inverseJoinColumns = @JoinColumn(name = "share_holder_id"))
	private Set<ShareHolder> shareHolders = new HashSet<>();

	public Project() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProjectVersion() {
		return projectVersion;
	}

	public void setProjectVersion(String projectVersion) {
		this.projectVersion = projectVersion;
	}

	public List<Bug> getBugs() {
		return bugs;
	}

	public void setBugs(List<Bug> bugs) {
		this.bugs = bugs;
	}
	
	public void addBug(Bug bug) {
		if(bug.getProject() != this) {
			bug.setProject(this);
		}
		bugs.add(bug);
	}
	
	public Set<ShareHolder> getShareHolders() {
		return shareHolders;
	}

	public void setShareHolders(Set<ShareHolder> shareHolders) {
		this.shareHolders = shareHolders;
	}

	public void addShareHolder(ShareHolder shareHolder) {
		if(!shareHolder.getProjects().contains(this)) {
			shareHolder.addProject(this);
		}
		shareHolders.add(shareHolder);
	}
	
	public void removeShareHolder(ShareHolder shareHolder) {
		this.shareHolders.remove(shareHolder);
		shareHolder.getProjects().remove(this);
	}
}
