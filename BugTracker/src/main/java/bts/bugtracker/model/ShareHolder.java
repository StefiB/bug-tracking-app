package bts.bugtracker.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity @Table(name = "tbl_share_holder")
public class ShareHolder {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String surname;
	@Column(unique = true, nullable = false)
	private String userName;
	@Column(nullable = false)
	private String password;
	@Column(nullable = false)
	private String role;
	
	@ManyToMany(mappedBy = "shareHolders", fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private Set<Project> projects = new HashSet<>();
	
	@OneToMany(mappedBy = "createdBy", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Bug> createdBugs = new ArrayList<>();
	
	@OneToMany(mappedBy = "closedBy", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Bug> closedBugs = new ArrayList<>();
	
	public ShareHolder() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void addProject(Project project) {
		if(!project.getShareHolders().contains(this)) {
			project.getShareHolders().add(this);
		}
		projects.add(project);
	}
	
	public void removeProject(Project project) {
		this.projects.remove(project);
		project.getShareHolders().remove(this);
	}

	public List<Bug> getCreatedBugs() {
		return createdBugs;
	}

	public void setCreatedBugs(List<Bug> createdBugs) {
		this.createdBugs = createdBugs;
	}

	public List<Bug> getClosedBugs() {
		return closedBugs;
	}

	public void setClosedBugs(List<Bug> closedBugs) {
		this.closedBugs = closedBugs;
	}

	public void addCreatedBug(Bug bug) {
		if(bug.getCreatedBy() != this) {
			bug.setCreatedBy(this);
		}
		
		createdBugs.add(bug);
	}
	
	public void addClosedBug(Bug bug) {
		if(bug.getClosedBy() != this) {
			bug.setClosedBy(this);
		}
		
		closedBugs.add(bug);
	}
}
