package bts.bugtracker.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity @Table(name = "tbl_bug")
public class Bug {

	private @Id @GeneratedValue Long id;
	private String title;
	@Column(columnDefinition = "TEXT") @Lob
	private String description;
	private Date created;
	@Column(nullable = true)
	private Date opened;
	@Column(nullable = true)
	private Date closed;
	
	private Severity severity;
	
	private Status status;
	
	private ClosedStatus closedStatus;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private ShareHolder createdBy;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private ShareHolder closedBy;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Project project;

	public Bug() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getOpened() {
		return opened;
	}

	public void setOpened(Date opened) {
		this.opened = opened;
	}

	public Date getClosed() {
		return closed;
	}

	public void setClosed(Date closed) {
		this.closed = closed;
	}

	public Severity getSeverity() {
		return severity;
	}

	public void setSeverity(Severity severity) {
		this.severity = severity;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public ShareHolder getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(ShareHolder createdBy) {
		this.createdBy = createdBy;
		if(!createdBy.getCreatedBugs().contains(this)) {
			createdBy.getCreatedBugs().add(this);
		}
	}

	public ShareHolder getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(ShareHolder closedBy) {
		this.closedBy = closedBy;
		if(!closedBy.getClosedBugs().contains(this)) {
			closedBy.getClosedBugs().add(this);
		}
	}

	public ClosedStatus getClosedStatus() {
		return closedStatus;
	}

	public void setClosedStatus(ClosedStatus closedStatus) {
		this.closedStatus = closedStatus;
	}
}
