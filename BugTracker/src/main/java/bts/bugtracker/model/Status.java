package bts.bugtracker.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Status {

	NEW("new"), IN_PROGRESS("in progress"), CLOSED("closed"), UNDEFINED("undefined");

	private String status;

	private Status(String status) {
		this.status = status;
	}

	public static List<Status> orderedValues;

	static {
		orderedValues = new ArrayList<>(Arrays.asList(Status.values()));
	}
	
	public Status nextStatus() {
		if(!this.equals(CLOSED)) {
			return orderedValues.get(this.ordinal() + 1);
		} else {
			throw new UnsupportedOperationException("Illegal request:\n\tImposible further STATUS iterations!");
		}
		
	}

	public static Status fromValue(String value) {
		for (Status st : values()) {
			if (st.status.equalsIgnoreCase(value)) {
				return st;
			} else if(st.status == null) {
				return UNDEFINED;
			}
		}

		return null;
		
//		throw new IllegalArgumentException(
//				"Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
	}

	public String getStatus() {
		return status;
	}
	
	public static List<Status> getOrderedValues() {
		List<Status> mod = new ArrayList<>(orderedValues);
		mod.remove(mod.size() - 1);
		return mod;
	}
}
