package bts.bugtracker.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class ShareholdersDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = -573944941747868244L;
	private String userName;
	private String password;
	private List<GrantedAuthority> authorities;
	
	public ShareholdersDetails(ShareHolder shareHolder) {
		this.userName = shareHolder.getUserName();
		this.password = shareHolder.getPassword();
		this.authorities = Arrays.stream(shareHolder.getRole().split(","))
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList());
	}
	
	public ShareholdersDetails() {}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return userName;
	}
	
	//The rest is hardcoded

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
