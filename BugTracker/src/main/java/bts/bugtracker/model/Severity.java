package bts.bugtracker.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Severity {

	LOW("low"), MEDIUM("medium"), HIGH("high"), UNDEFINED("undefined");
	
	private String severity;
	
	private Severity(String severity) {
		this.severity = severity;
	}
	
	
	public static List<Severity> orderedValues = new ArrayList<>();
	
	static {
		orderedValues.addAll(Arrays.asList(Severity.values()));
	}
	
	
	public static Severity fromValue(String value) {
		for(Severity sev : values()) {
			if(sev.severity.equalsIgnoreCase(value)) {
				return sev;
			} else if(sev.severity == null) {
				return UNDEFINED;
			}
		}
		return null;
		
//		throw new IllegalArgumentException(
//					"Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values())
//				);
	}


	public String getSeverity() {
		return severity;
	}
	
	public static List<Severity> getOrderedValues() {
		List<Severity> mod = new ArrayList<>(orderedValues);
		mod.remove(mod.size() - 1);
		return mod;
	}
}
