CREATE USER IF NOT EXISTS bug_tracker_admin IDENTIFIED BY 'admin';

DROP DATABASE IF EXISTS bugtracker_db;
CREATE DATABASE bugtracker_db DEFAULT CHARACTER SET utf8;

USE bugtracker_db;

GRANT ALL ON bugtracker_db.* TO 'bug_tracker_admin'@'%';

FLUSH PRIVILEGES;